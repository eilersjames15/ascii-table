use std::fmt::Display;
use ascii_table::{AsciiTable, Align};

fn main(){
let mut chess_board = AsciiTable::default();
//chess_board.set_max_width(1000);
let mut chess_board = AsciiTable::default();
chess_board.set_max_width(37);
chess_board.column(0).set_max_width(5).set_header(" ");
chess_board.set_min_width(0, 37);
let letters = ['a','b','c','d','e','f','g','h'];
for i in 1..=8 {
    let header = letters[i-1] as char; // Calculate the header from 'a' to 'h'
    chess_board.column(i).set_header(header).set_align(Align::Left);
}

let white_king = '\u{2654}';
let white_queen = '\u{2655}';
let white_rook = '\u{2656}';
let white_bishop = '\u{2657}';
let white_knight = '\u{2658}';
let white_pawn = '\u{2659}';
let black_king = '\u{265A}';
let black_queen = '\u{265B}';
let black_rook = '\u{265C}';
let black_bishop = '\u{265D}';
let black_knight = '\u{265E}';
let black_pawn = '\u{265F}';

let empty_square = " "; // A space character as placeholder
let set_board: Vec<Vec<&dyn Display>> = vec![
    vec![&'8',&white_rook,&white_knight,&white_bishop,&white_queen,&white_king,&white_bishop,&white_knight,&white_rook],
    vec![&'7',&white_pawn,&white_pawn,&white_pawn,&white_pawn,&white_pawn,&white_pawn,&white_pawn,&white_pawn],
    vec![&'6', &empty_square, &empty_square, &empty_square, &empty_square, &empty_square, &empty_square, &empty_square, &empty_square],
    vec![&'5', &empty_square, &empty_square, &empty_square, &empty_square, &empty_square, &empty_square, &empty_square, &empty_square],
    vec![&'4', &empty_square, &empty_square, &empty_square, &empty_square, &empty_square, &empty_square, &empty_square, &empty_square],
    vec![&'3', &empty_square, &empty_square, &empty_square, &empty_square, &empty_square, &empty_square, &empty_square, &empty_square],
    vec![&'2',&black_pawn,&black_pawn,&black_pawn,&black_pawn,&black_pawn,&black_pawn,&black_pawn,&black_pawn],
    vec![&'1',&black_rook,&black_knight,&black_bishop,&black_king,&black_queen,&black_bishop,&black_knight,&black_rook],
];

chess_board.print(set_board);
}
