# ascii-table

Print ASCII tables to the terminal.

# Forked version

Trying to add minimum width so I can create a chessboard with this crate

can run this with the following and ge the output

PS D:\chessh\ascii-table> cargo run --features "wide_characters"
warning: method `width` is never used
   --> src\lib.rs:632:8
    |
620 | impl SmartStringFragment {
    | ------------------------ method in this implementation
...
632 |     fn width(&self) -> usize {
    |        ^^^^^
    |
    = note: `#[warn(dead_code)]` on by default

warning: `ascii_table` (lib) generated 1 warning
warning: unused variable: `chess_board`
 --> src\main.rs:5:9
  |
5 | let mut chess_board = AsciiTable::default();
  |         ^^^^^^^^^^^ help: if this is intentional, prefix it with an underscore: `_chess_board`
  |
  = note: `#[warn(unused_variables)]` on by default

warning: variable does not need to be mutable
 --> src\main.rs:5:5
  |
5 | let mut chess_board = AsciiTable::default();
  |     ----^^^^^^^^^^^
  |     |
  |     help: remove this `mut`
  |
  = note: `#[warn(unused_mut)]` on by default

warning: `ascii_table` (bin "ascii_table") generated 2 warnings (run `cargo fix --bin "ascii_table"` to apply 2 suggestions)
    Finished dev [unoptimized + debuginfo] target(s) in 0.01s
     Running `target\debug\ascii_table.exe`
┌───┬───┬───┬───┬───┬───┬───┬───┬───┐
│   │ a │ b │ c │ d │ e │ f │ g │ h │
├───┼───┼───┼───┼───┼───┼───┼───┼───┤
│ 8 │ ♖ │ ♘ │ ♗ │ ♕ │ ♔ │ ♗ │ ♘ │ ♖ │
├─┼─┼─┼─┼─┼─┼─┼─┼─┤
│ 7 │ ♙ │ ♙ │ ♙ │ ♙ │ ♙ │ ♙ │ ♙ │ ♙ │
├─┼─┼─┼─┼─┼─┼─┼─┼─┤
│ 6 │   │   │   │   │   │   │   │   │
├─┼─┼─┼─┼─┼─┼─┼─┼─┤
│ 5 │   │   │   │   │   │   │   │   │
├─┼─┼─┼─┼─┼─┼─┼─┼─┤
│ 4 │   │   │   │   │   │   │   │   │
├─┼─┼─┼─┼─┼─┼─┼─┼─┤
│ 3 │   │   │   │   │   │   │   │   │
├─┼─┼─┼─┼─┼─┼─┼─┼─┤
│ 2 │ ♟ │ ♟ │ ♟ │ ♟ │ ♟ │ ♟ │ ♟ │ ♟ │
├─┼─┼─┼─┼─┼─┼─┼─┼─┤
│ 1 │ ♜ │ ♞ │ ♝ │ ♚ │ ♛ │ ♝ │ ♞ │ ♜ │
└───┴───┴───┴───┴───┴───┴───┴───┴───┘
PS D:\chessh\ascii-table>

## Example

```
use ascii_table::AsciiTable;

let ascii_table = AsciiTable::default();
let data = vec![&[1, 2, 3], &[4, 5, 6], &[7, 8, 9]];
ascii_table.print(data);
// ┌───┬───┬───┐
// │ 1 │ 2 │ 3 │
// │ 4 │ 5 │ 6 │
// │ 7 │ 8 │ 9 │
// └───┴───┴───┘
```

## Example

```
use std::fmt::Display;
use ascii_table::{AsciiTable, Align};

let mut ascii_table = AsciiTable::default();
ascii_table.set_max_width(26);
ascii_table.column(0).set_header("H1").set_align(Align::Left);
ascii_table.column(1).set_header("H2").set_align(Align::Center);
ascii_table.column(2).set_header("H3").set_align(Align::Right);

let data: Vec<Vec<&dyn Display>> = vec![
    vec![&'v', &'v', &'v'],
    vec![&123, &456, &789, &"abcdef"]
];
ascii_table.print(data);
// ┌─────┬─────┬─────┬──────┐
// │ H1  │ H2  │ H3  │      │
// ├─────┼─────┼─────┼──────┤
// │ v   │  v  │   v │      │
// │ 123 │ 456 │ 789 │ abc+ │
// └─────┴─────┴─────┴──────┘
```

## Features

- `auto_table_width`: Sets the default max width of the ascii table to the width of the terminal.
- `color_codes`: Correctly calculates the width of a string when terminal color codes are present
  (like those from the `colorful` crate).
- `wide_characters`: Correctly calculates the width of a string when wide characters are present
  (like emoli's).
